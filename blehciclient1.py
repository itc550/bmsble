from __future__ import print_function

import sys
import time
from gattlib import GATTRequester

class Reader(object):
    def __init__(self, address):
        self.requester = GATTRequester(address, False)
        self.connect()
        for i in range(5):
            #self.connect()
            self.request_data()
            #self.disconnect()


    def connect(self):
        print("Connecting...", end=' ')
        sys.stdout.flush()

        self.requester.connect(True)
        print("OK!")

    def disconnect(self):
        print("Disconnecting...", end=' ')
        sys.stdout.flush()

        self.requester.disconnect()
        print("OK!\n")

        time.sleep(1)

    def request_data(self):
        #data = self.requester.read_by_uuid("00002a00-0000-1000-8000-00805f9b34fb")[0]
        #print("From: "+data)

        data = self.requester.read_by_uuid("2a19")[0]

        print("Received:", end=' ')
        for b in data:
            print(str(int(hex(ord(b)),16)), end=' ')
        print("")


if __name__ == '__main__':

    #Reader("B8:27:")
    Reader("B8:27:EB:0E:E2:B7")
    print("Done.")
