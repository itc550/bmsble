import platform
from time import sleep
import bluetooth._bluetooth as bluez
from random import randint

from cell_utils import (start_le_advertising,
                             stop_le_advertising)

from cell_utils import (toggle_device,
                             enable_le_scan, parse_le_advertising_events,
                             disable_le_scan, raw_packet_to_str)

platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)
mac_addr="B8:27:EB:9B:F4:40"
mac_name="Controller"
ref = 0
dev_id = 0  # the bluetooth device is hci0
soc = 100
old_ref = ref
randsim = randint(1,2)
statusval = True

if (platform_check==platform_ref):
    print("Setting up GPIO")
    import RPi.GPIO as GPIO

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(3, GPIO.OUT)
    GPIO.output(3, GPIO.HIGH)

def socsim(soc):
    soc -= randsim
    return  soc

def status(soc, ref):
    if(soc >= ref):
        if (platform_check == platform_ref):
            GPIO.output(3, GPIO.HIGH)
        return True
    if(soc < ref):
        if (platform_check == platform_ref):
            GPIO.output(3, GPIO.LOW)
        return False

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise

def le_advertise_packet_handler(mac, data, rssi):
    global prev_data
    global ref
    global statusval
    global old_ref
    data_str = raw_packet_to_str(data)
    data_str = int(str(data_str[2:]), 16)
    if(mac_addr==mac):
        ref = data_str
        print(mac_name+": %s %s" % (mac, data_str))



toggle_device(dev_id, True)
enable_le_scan(sock,0x05,0x05, filter_duplicates=False)
try:
        for i in range(0,100):
            if (statusval == True):
                soc = socsim(soc)
            start_le_advertising(sock,
                                 min_interval=100, max_interval=150,
                                 data= (soc,))
            sleep(2)
            #stop_le_advertising(sock)

            parse_le_advertising_events(1,sock,
                                        handler=le_advertise_packet_handler,
                                        debug=False)
      
            #disable_le_scan(sock)
            i=i+1
            if (ref != old_ref):
                statusval = status(soc, ref)
                old_ref = ref

            print("Latest reference: ", ref)
            print("Charging status:", statusval)

except:
    stop_le_advertising(sock)
    disable_le_scan(sock)
    GPIO.cleanup()
    raise

stop_le_advertising(sock)
disable_le_scan(sock)
GPIO.cleanup()