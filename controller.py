"""
Simple BLE advertisement example
"""
import platform
from time import sleep
import bluetooth._bluetooth as bluez

from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

from bluetooth_utils import (toggle_device,
                             enable_le_scan, parse_le_advertising_events,
                             disable_le_scan, raw_packet_to_str)

platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
if (platform_check==platform_ref):
    print("Setting up GPIO")
    import RPi.GPIO as GPIO

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(3, GPIO.OUT)

dev_id = 0  # the bluetooth device is hci0
toggle_device(dev_id, True)

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise

mac_addr="B8:27:EB:86:DB:84",   "B8:27:EB:10:84:75",   "B8:27:EB:28:5F:0F",     "B8:27:EB:0E:E2:B7",    "B8:27:EB:BF:60:E3",    "B8:27:EB:BE:03:43",    "B8:27:EB:AC:C8:6E",    "B8:27:EB:62:AF:40",    "B8:27:EB:65:27:F3",    "B8:27:EB:C4:B9:C0",     "48:51:B7:37:B1:34"#,   "9C:B6:D0:B8:08:E4",
mac_name="Rpi 2",               "Rpi 3",               "Rpi 4",                 "Rpi 5",                "Rpi 6",                "Rpi 7",                "Rpi 8",                "Rpi 9",                "Rpi 10",              "Rpi 11",                "Stempeldiller"#,       "Rolf"
active_addr=[]
toggle=[0]

raw=[90,90,90,90,90,90,90,90,90,90,90,90,90,90,90]

def broadcast_data(data):
    sum=0
    for i in range(len(active_addr)):
        sum=sum+data[i]
    if (len(active_addr)!=0):
        average=sum/len(active_addr)
    else:
        average=sum
    average =int(round(average))
    return average

def toggle_led():
    if (platform_check == platform_ref):
        if toggle[0]==1:
            GPIO.output(3, GPIO.HIGH)
            toggle[0]=0
        else:
            GPIO.output(3, GPIO.LOW)
            toggle[0]=1


def le_advertise_packet_handler(mac, data, rssi):
    x=0
    index=0
    global prev_data
    data_str = raw_packet_to_str(data)
    data_str = int(str(data_str[2:]), 16)
    for i in range(len(mac_addr)):
        if (mac==mac_addr[i]):
            if ((mac in active_addr)==False):
                active_addr.append(mac)
                print("     New MAC address: ",mac)
    for i in range (len(active_addr)):
        if(active_addr[i]==mac):
            raw[i] = data_str
            name_index=mac_addr.index(mac)
            print(mac_name[name_index] + ": %s %s" % (mac, data_str))
            toggle_led()

enable_le_scan(sock, 0x05, 0x05, filter_duplicates=False)

try:
        for i in range(0,25):
            pck=broadcast_data(raw)
            print("Broadcasting: ", pck)
            start_le_advertising(sock,
                                 min_interval=32, max_interval=32,
                                 data= (pck,))
            sleep(5)
            #stop_le_advertising(sock)


            parse_le_advertising_events(15,sock,
                                        handler=le_advertise_packet_handler,
                                        debug=False)
            #disable_le_scan(sock)
            print("# of active MAC´s: ",len(active_addr))
            print(raw[0:(len(active_addr))])
            i=i+1

except:
    stop_le_advertising(sock)
    disable_le_scan(sock)
    if (platform_check == platform_ref):
        GPIO.cleanup()
    raise

stop_le_advertising(sock)
disable_le_scan(sock)
if (platform_check == platform_ref):
    GPIO.cleanup()
