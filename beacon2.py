"""
Simple BLE advertisement example
"""
from time import sleep
import bluetooth._bluetooth as bluez

from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

from bluetooth_utils import (toggle_device,
                             enable_le_scan, parse_le_advertising_events,
                             disable_le_scan, raw_packet_to_str)

dev_id = 0  # the bluetooth device is hci0
toggle_device(dev_id, True)

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise

mac_addr="48:51:B7:37:B1:34","B8:27:EB:9B:F4:40","9C:B6:D0:B8:08:E4","B8:27:EB:86:DB:84"
mac_name="Stempeldiller","Rpi_1","Rolf","Rpi 2"

def le_advertise_packet_handler(mac, data, rssi):
    x=0
    index=0
    global prev_data
    data_str = raw_packet_to_str(data)
    for i in range (4):
        if(mac_addr[i]==mac):
            index=i
            x=1
    if (x):
        print(mac_name[index]+": %s %s" % (mac, int(str(data_str[2:]), 16)))



try:
        for i in range(0,5):
            start_le_advertising(sock,
                                 min_interval=2000, max_interval=2000,
                                 data= (69,))
            sleep(0.25)
            stop_le_advertising(sock)

            enable_le_scan(sock, 0x20, 0x20, filter_duplicates=False)
            parse_le_advertising_events(0.1,sock,
                                        handler=le_advertise_packet_handler,
                                        debug=False)
            disable_le_scan(sock)
            i=i+1

except:
    stop_le_advertising(sock)
    raise
